package com.keng.common.util.time;

import java.time.ZoneOffset;

public enum ZoneOffsetEnum {

    CTT(8, "Asia/Shanghai"),
    UTF_8(CTT),

    ;

    private int zone;
    private String zoneId;
    private ZoneOffset zoneOffset;

    ZoneOffsetEnum(ZoneOffsetEnum zoneOffsetEnums) {
        this(zoneOffsetEnums.zone, zoneOffsetEnums.zoneId, zoneOffsetEnums.zoneOffset);
    }

    ZoneOffsetEnum(int zone, String zoneId) {
        this.zone = zone;
        this.zoneId = zoneId;
        zoneOffset = ZoneOffset.ofHours(8);
    }

    ZoneOffsetEnum(int zone, String zoneId, ZoneOffset zoneOffset) {
        this.zone = zone;
        this.zoneId = zoneId;
        this.zoneOffset = zoneOffset;
    }

    public ZoneOffset getZoneOffset() {
        return zoneOffset;
    }
}
