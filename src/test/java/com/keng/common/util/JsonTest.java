package com.keng.common.util;

import com.keng.common.util.json.JsonUtil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class JsonTest {

    @Test
    public void tomapTest() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "mkyong");
        map.put("age", 29);

        String s = JsonUtil.toString(map, true);
        System.out.println(s);
    }

}
