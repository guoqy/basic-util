package com.keng.common.util.sentry;

import org.apache.commons.lang3.StringUtils;

public final class SentryUtil {

    public static final String SENTRY_FILE_SYSTEM_PROPERTY          = "sentry.properties.file";
    public static final String SENTRY_FILE_SYSTEM_ENV               = "SENTRY_PROPERTIES_FILE";
    public static final String SENTRY_FILE_SPRING_BOOT_PROPERTY     = SENTRY_FILE_SYSTEM_PROPERTY;
    public static final String SENTRY_DEFAULT_CONFIG_FILE           = "sentry.properties";

    public static String getSentryConfigFileByProfile(String profile) {
        if (StringUtils.isBlank(profile)) {
            return "";
        }

        return SENTRY_DEFAULT_CONFIG_FILE.replace("\\.", "-" + SENTRY_DEFAULT_CONFIG_FILE + ".");
    }
}
