package com.keng.common.util.json.deser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonTokenId;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.keng.common.util.time.ZoneOffsetEnum;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeMillisDeserializer extends LocalDateTimeDeserializer {


    public LocalDateTimeMillisDeserializer(DateTimeFormatter formatter) {
        super(formatter);
    }

    @Override
    public LocalDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        if (parser.hasTokenId(JsonTokenId.ID_NUMBER_INT)) {
            String millis = parser.getText().trim();
            return Instant.ofEpochMilli(Long.valueOf(millis)).atOffset(ZoneOffsetEnum.CTT.getZoneOffset()).toLocalDateTime();
        }
        return super.deserialize(parser, context);
    }
}
