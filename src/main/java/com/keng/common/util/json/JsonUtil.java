package com.keng.common.util.json;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.keng.common.util.json.deser.LocalDateTimeMillisDeserializer;
import com.keng.common.util.json.ser.LocalDateTimeMillisSerializer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class JsonUtil {

    private static final ObjectMapper OBJECT_MAPPER;

    static {
        OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        OBJECT_MAPPER.enable(new JsonParser.Feature[]{JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES});

        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(LocalDate.class, LocalDateSerializer.INSTANCE);
        javaTimeModule.addDeserializer(LocalDate.class, LocalDateDeserializer.INSTANCE);

        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeMillisSerializer());
        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeMillisDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        OBJECT_MAPPER.registerModule(javaTimeModule);

        // objectMapper.configure( SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, true );
        // objectMapper.configure( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true );
        OBJECT_MAPPER.enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    public synchronized static void registerModule(Module... modules) {
        OBJECT_MAPPER.registerModules(modules);
    }

    public synchronized static void registerModule(Iterable<Module> moduleIterator) {
        OBJECT_MAPPER.registerModules(moduleIterator);
    }

    public static String toString(Object o) {
        return toString(null, o);
    }

    public static String toString(Class<?> serView, Object o) {
        return toString(serView, o, false);
    }

    public static String toString(Object o, boolean isPretty) {
        return toString(null, o, isPretty);
    }

    public static String toString(Class<?> serView, Object o, boolean isPretty) {
        if (Objects.nonNull(o)) {
            try {
                ObjectWriter objectWriter = OBJECT_MAPPER.writer();

                if (isPretty) {
                    objectWriter = objectWriter.withDefaultPrettyPrinter();
                }

                if (serView == null) {
                    objectWriter = objectWriter.withView(serView);
                }

                return objectWriter.writeValueAsString(o);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static <T> T toObject(Class<T> clazz, String json) {
        if (isNotEmpty(json)) {
            return null;
        }
        try {
            return OBJECT_MAPPER.readValue(json, clazz);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static <T> T toObject(Class<T> clazz, byte[] data) {
        if (clazz != null && isNotEmpty(data)) {
            try {
                return OBJECT_MAPPER.readValue(data, clazz);
            } catch (Throwable var3) {
                var3.printStackTrace();
            }
        }

        return null;
    }

    public static <T> List<T> toList(Class<T> clazz, String json) {
        if (clazz != null && isNotEmpty(json)) {
            try {
                return (List)OBJECT_MAPPER.readValue(json, OBJECT_MAPPER.getTypeFactory().constructCollectionType(ArrayList.class, clazz));
            } catch (Throwable var3) {
                var3.printStackTrace();
            }
        }

        return Collections.emptyList();
    }

    public static <K, V> Map<K, V> toMap(String json) {
        if (isNotEmpty(json)) {
            try {
                return (Map)OBJECT_MAPPER.readValue(json, new TypeReference<Map<K, V>>() {
                });
            } catch (Throwable var2) {
                var2.printStackTrace();
            }
        }

        return Collections.emptyMap();
    }

    public static <K, V> Map<K, V> toMap(byte[] data) {
        if (isNotEmpty(data)) {
            try {
                return (Map)OBJECT_MAPPER.readValue(data, new TypeReference<Map<K, V>>(){});
            } catch (Throwable var2) {
                var2.printStackTrace();
            }
        }

        return Collections.emptyMap();
    }

    public static JsonNode toJsonNode(String json) {
        if (isNotEmpty(json)) {
            try {
                return OBJECT_MAPPER.readTree(json);
            } catch (Throwable var2) {
                var2.printStackTrace();
            }
        }

        return null;
    }

    public static JsonNode toJsonNode(String json, String fieldName) {
        if (isNotEmpty(json) && isNotEmpty(fieldName)) {
            try {
                JsonNode jsonNode = OBJECT_MAPPER.readTree(json);
                return jsonNode.findPath(fieldName);
            } catch (Throwable var3) {
                var3.printStackTrace();
            }
        }

        return null;
    }

    private static boolean isNotEmpty(String s) {
        return s != null && s.length() > 0;
    }

    private static boolean isNotEmpty(byte[] data) {
        return data != null && data.length > 0;
    }


    public static void main(String[] args) {

        P p = new P();
        p.setCreateTime(LocalDateTime.now());

        String s = JsonUtil.toString(p);

        System.out.println(s);

        System.out.println(JsonUtil.toString(new Date()));

        P p1 = JsonUtil.toObject(P.class, s);

        System.out.println(p1.getCreateTime());

        String json = "{\"createTime\": \"2018-12-10 10:09:34\"}";

        P p2 = JsonUtil.toObject(P.class, json);

        System.out.println(p2.getCreateTime());
    }

    static class P {
        @JsonFormat(pattern = "", timezone = "")
        private LocalDateTime createTime;

        public LocalDateTime getCreateTime() {
            return createTime;
        }

        public void setCreateTime(LocalDateTime createTime) {
            this.createTime = createTime;
        }
    }

}
