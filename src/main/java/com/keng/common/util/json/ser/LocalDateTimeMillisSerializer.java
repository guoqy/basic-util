package com.keng.common.util.json.ser;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeMillisSerializer extends LocalDateTimeSerializer {

    @Override
    public void serialize(LocalDateTime value, JsonGenerator g, SerializerProvider provider) throws IOException {

        if (_formatter == null) {
            if (value == null) {
                g.writeNumber(0);
            } else {
                g.writeNumber(value.toInstant(ZoneOffset.ofHours(8)).toEpochMilli());
            }
        }else {
            super.serialize(value, g, provider);
        }
    }
}
